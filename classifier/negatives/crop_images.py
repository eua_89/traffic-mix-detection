import cv2
import os
import glob

image = cv2.imread("pictures1.bmp")
(winW, winH) = (256, 256) 
windowSize=(winW, winH)
stepSize=40
i=0

for image in glob.glob('*.bmp'):
    image = cv2.imread(image)
    
    for y in xrange(0, image.shape[0], stepSize):
            for x in xrange(0, image.shape[1], stepSize):
                i=i+1
                # yield the current window
                crop_img = image[y:y + windowSize[1], x:x + windowSize[0]]
                if (crop_img.shape[0]==256 and crop_img.shape[1]==256):
                    cv2.imshow("cropped", crop_img)
                    #cv2.imwrite('image_{}.jpg'.format(i),crop_img)
                    cv2.imwrite(os.path.join('windows_256_256_40','image_{}.jpg'.format(i)),crop_img)
                






















